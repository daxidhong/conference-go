# Generated by Django 4.0.3 on 2023-03-30 18:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attendees', '0005_alter_accountvo_email_alter_accountvo_first_name_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accountvo',
            name='is_active',
            field=models.BooleanField(),
        ),
        migrations.AlterField(
            model_name='accountvo',
            name='updated',
            field=models.DateTimeField(),
        ),
    ]
