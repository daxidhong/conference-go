import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    params = {"per_page": 1, "query": city + " " + state}
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    # look at the pexel documentation for this
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}

    # figure out how to get weather for pre exisiting locations!


def get_weather_data(city, state):
    geocode_url = "http://api.openweathermap.org/geo/1.0/direct"
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    geo_params = {
        "q": f"{city},{state},{1}",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(geocode_url, params=geo_params, headers=headers)
    content = json.loads(response.content)
    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    weather_params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(
        weather_url, params=weather_params, headers=headers
    )
    content = json.loads(response.content)
    try:
        weather = {
            "temperature": content["main"]["temp"],
            "description": content["weather"][0]["description"],
        }
        return weather
    except (KeyError, IndexError):
        return None
