# this file exists to hold our code
from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet

# a QuerySet is a fancy list tied to data in the database
# we want to turn the fancy QuerySet into a list
class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        # if o is an instance of a QuerySet --> turn it into a normal list rather
        # than the fancy list it already is
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class DateEncoder(JSONEncoder):
    def default(self, o):
        # if o is an instance of datetime:
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)
        #    return super().default(o)


# we want ModelEncoder to have the functionality of DateEncoder, so add it to ModelEncoder
# inheritance list
class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    # from ConferenceDetailEncoder in events/views.py
    encoders = {}

    def default(self, o):
        # if the object to decode is the same class as what's in the model property, then:
        # this checks if incoming object stored in o parameter is an instance of the class
        # stored in the self.model property
        if isinstance(o, self.model):

            # create an empty dictionary that will hold the property names
            # as keys and the property values as values
            d = {}

            # if o has the attribute get_api_url:
            if hasattr(o, "get_api_url"):
                # then add its return value to the dictionary with the key "href"
                d["href"] = o.get_api_url()

            # * for each name in the properties list:
            for property in self.properties:
                # get the value of that property from the model instance given just the property name
                # getattr gets the value of property of the object by its name
                # the name is stored in the variable defined by the for loop
                value = getattr(o, property)

                # from ConferenceDetailEncoder in events/views.py
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)

                # put it into the dictionary with that property name as the key
                d[property] = value

                d.update(self.get_extra_data(o))

            # return the dictionary
            return d

        # otherwise:
        # this is just boilerplate code from the documentation
        # return super().default(o)  # From the documentation
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}
