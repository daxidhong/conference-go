import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


## this is all dahnas code
def presentation_approval(ch, method, properties, body):
    print("Received %r" % body)
    context = json.loads(body)
    presenter_name = context["presenter_name"]
    title = context["title"]
    presenter_email = context["presenter_email"]
    recipient_list = [presenter_email]
    send_mail(
        "Your presentation has been accepted",
        f"{presenter_name}, we are happy to tell you that your presentation {title} has been accepted",
        "admin@conference.go",
        recipient_list,
        fail_silently=False,
    )
    print("sent approval letter")


def presentation_rejection(ch, method, properties, body):
    print("Received %r" % body)
    context = json.loads(body)
    presenter_name = context["presenter_name"]
    title = context["title"]
    presenter_email = context["presenter_email"]
    recipient_list = [presenter_email]
    send_mail(
        "Your presentation has not been approved",
        f"{presenter_name}, we are sad to tell you that your presentation {title} has not been accepted",
        "admin@conference.go",
        recipient_list,
        fail_silently=False,
    )
    print("sent rejection letter")


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()

        channel.queue_declare(queue="approvedtask")
        channel.queue_declare(queue="rejectedtask")

        channel.basic_consume(
            queue="approvedtask",
            on_message_callback=presentation_approval,
            auto_ack=True,
        )
        channel.basic_consume(
            queue="rejectedtask",
            on_message_callback=presentation_rejection,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)


## this is all my code
# def process_approval(ch, method, properties, body):
#     body = json.loads(body)
#     presenter_name = body["presenter_name"]
#     title = body["title"]
#     presenter_email = body["presenter_email"]
#     send_mail(
#         "Your presentation has been accepted",
#         f"{presenter_name}, we're happy to tell you that your presentation {title} has been accepted",
#         "admin@conference.go",
#         [presenter_email],
#         fail_silently=False,
#     )
#     print("sent approval letter")


# # parameters = pika.ConnectionParameters(host="rabbitmq")
# # connection = pika.BlockingConnection(parameters)
# # channel = connection.channel()


# def process_rejection(ch, method, properties, body):
#     body = json.loads(body)
#     presenter_name = body["presenter_name"]
#     title = body["title"]
#     presenter_email = body["presenter_email"]
#     send_mail(
#         "Your presentation has been rejected",
#         f"{presenter_name}, we're sorry to tell you that your presentation {title} has been rejected",
#         "admin@conference.go",
#         [presenter_email],
#         fail_silently=False,
#     )
#     print("sent rejection letter")


# parameters = pika.ConnectionParameters(host="rabbitmq")
# connection = pika.BlockingConnection(parameters)
# channel = connection.channel()

# channel.queue_declare(queue="presentation_approvals")
# channel.basic_consume(
#     queue="presentation_approvals",
#     on_message_callback=process_approval,
#     auto_ack=True,
# )

# channel.queue_declare(queue="presentation_rejections")
# channel.basic_consume(
#     queue="presentation_rejections",
#     on_message_callback=process_rejection,
#     auto_ack=True,
# )
# channel.start_consuming()

# # so far, only approval or rejection emails are coming into the inbox
# # run these to test again
# # 1. docker run -d  --name rabbitmq --hostname rabbitmq --network conference-go rabbitmq:3
# # 2. docker run -d -v "$(pwd)/monolith:/app" -p 8000:8000 --network conference-go conference-go-dev
# # 3. docker run -d -p "3000:8025" -e "MH_SMTP_BIND_ADDR=0.0.0.0:25" --name mail --network conference-go mailhog/mailhog
# # 4. docker run -it --network conference-go -v "$(pwd)/presentation_workflow:/app" presentation-workflow-dev bash
# # 5. python presentation_mailer/consumer.py
# # 6. request in insomnia
